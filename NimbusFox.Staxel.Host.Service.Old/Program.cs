﻿using System.ServiceProcess;

namespace NimbusFox.Staxel.Host.Service {
    static class Program {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main() {
            ServiceBase[] ServicesToRun;
            ServicesToRun = new ServiceBase[]
            {
                new StaxelHostService()
            };
            ServiceBase.Run(ServicesToRun);
        }
    }
}
