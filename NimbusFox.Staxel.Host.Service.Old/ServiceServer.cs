﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using NamedPipeWrapper;
using NimbusFox.Staxel.Host.Shared;
using Constants = NimbusFox.Staxel.Host.Shared.Constants;

namespace NimbusFox.Staxel.Host.Service {
    internal class ServiceServer : IDisposable {
        private NamedPipeServer<Blob> _server;
        public void Dispose() { }

        public ServiceServer() {
            _server = new NamedPipeServer<Blob>(StaxelHostService.Settings.PipeName);

            _server.ClientMessage += _server_ClientMessage;

            _server.Start();
        }

        private void _server_ClientMessage(NamedPipeConnection<Blob, Blob> connection, Blob blob) {
            try {
                var type = blob.GetString("type");
                if (type == Constants.MessageCreateServer) {
                    CreateServer(blob.GetBlob("data"));
                } else if (type == Constants.MessageDeleteServer) {
                    RemoveServer(blob.GetBlob("data"));
                } else if (type == Constants.MessageUpdateServer) {
                    UpdateServer(blob.GetBlob("data"));
                } else if (type == Constants.MessageUpdateServerConfig) {
                    UpdateServerConfig(blob.GetBlob("data"));
                }
            } catch (Exception ex) {
                StaxelHostService.Log.Write(ex.Message + Environment.NewLine + ex.StackTrace, Logger.LogType.Error);
            }
        }

        private void CreateServer(Blob data) {
            if (new List<ServerInstance>(StaxelHostService.Servers).Any(x => x.ID.ToString() == data.GetString("id"))) {
                StaxelHostService.Log.Write($"Unable to create server for {data.GetString("directory")} as it shares an id with another server", Logger.LogType.Error);
                return;
            }

            var instance = new ServerInstance(data, this);
            StaxelHostService.Servers.Add(instance);
            instance.SaveConfig();
        }

        private void RemoveServer(Blob data) {
            foreach (var instance in new List<ServerInstance>(StaxelHostService.Servers)) {
                if (instance.ID.ToString() == data.GetString("id")) {
                    instance.Stop();

                    File.Delete(Path.Combine(Constants.InstancesDirectory, instance.ID + ".instance"));
                    StaxelHostService.Servers.Remove(instance);
                }
            }
        }

        private void UpdateServer(Blob data) {
            foreach (var instance in new List<ServerInstance>(StaxelHostService.Servers)) {
                if (instance.ID.ToString() == data.GetString("id")) {
                    instance.UpdateStaxel();
                }
            }
        }

        private void UpdateServerConfig(Blob data) {
            foreach (var instance in new List<ServerInstance>(StaxelHostService.Servers)) {
                if (instance.ID.ToString() == data.GetString("id")) {
                    instance.UpdateConfig(data);
                    instance.SaveConfig();
                }
            }
        }

        public void SendBlob(Blob data) {
            _server.PushMessage(data);
        }
    }
}
