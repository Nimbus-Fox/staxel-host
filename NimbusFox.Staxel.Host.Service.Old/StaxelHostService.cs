﻿using System;
using System.Collections.Generic;
using System.IO;
using System.ServiceProcess;
using NimbusFox.Staxel.Host.Shared;

namespace NimbusFox.Staxel.Host.Service {
    public partial class StaxelHostService : ServiceBase {
        internal static ServiceSettings Settings { get; private set; }
        internal static Logger Log => new Logger(Path.Combine(Constants.BinDirectory, "ServiceLogs"));

        internal static List<ServerInstance> Servers { get; } = new List<ServerInstance>();
        private ServiceServer _server;

        public StaxelHostService() {
            using (var config = Blob.ReadJson(File.ReadAllText(Path.Combine(Constants.BinDirectory, "service.cfg")))) {
                Settings = new ServiceSettings(config);
            }

            if (!Directory.Exists(Constants.InstancesDirectory)) {
                Directory.CreateDirectory(Constants.InstancesDirectory);
            }

            InitializeComponent();
        }

        protected override void OnStart(string[] args) {
            if (!Settings.ValidSettings) {
                Log.Write("Unable to start Host service due to invalid configs. Please check the service.cfg file", Logger.LogType.Error);
                return;
            }

            try {
                _server = new ServiceServer();
            } catch (Exception ex) {
                Log.Write(ex.Message + Environment.NewLine + ex.Message, Logger.LogType.Error);
            }

            foreach (var file in new DirectoryInfo(Constants.InstancesDirectory).GetFiles("*.instance")) {
                try {
                    var blob = Blob.ReadJson(File.ReadAllText(file.FullName));
                    Servers.Add(new ServerInstance(blob, _server));
                } catch (Exception ex) {
                    Log.Write(ex.Message + Environment.NewLine + ex.StackTrace, Logger.LogType.Error);
                }
            }
        }

        protected override void OnStop() {
            if (!Settings.ValidSettings) {
                return;
            }

            foreach (var instance in Servers) {
                instance.Stop();
            }

            _server.Dispose();
        }
    }
}
