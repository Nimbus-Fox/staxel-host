﻿using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using NimbusFox.Staxel.Host.Shared;

namespace NimbusFox.Staxel.Host.Service {
    internal class ServerInstance {

        private string Directory { get; }
        private bool _onStartup;
        private string _commandArgs;
        private int[] _workshopMods;
        private bool _updateModsOnStartup;
        private bool _updateStaxelOnStartup;
        private string _name;
        public Guid ID { get; }

        private ServiceServer Server { get; }

        private Process _process;
        private bool _canCancel;

        public ServerInstance(Blob instanceData, ServiceServer server) {
            Server = server;
            ID = Guid.Parse(instanceData.GetString("id"));
            Directory = Path.Combine(StaxelHostService.Settings.ServerStorageLocation, ID.ToString());
            _name = instanceData.GetString("name");
            _onStartup = instanceData.GetBool("onStartup");
            _commandArgs = instanceData.GetString("commandArgs");
            _workshopMods = instanceData.GetList<int>("workshopMods");
            _updateModsOnStartup = instanceData.GetBool("updateModsOnStartup");
            _updateStaxelOnStartup = instanceData.GetBool("updateStaxelOnStartup");

            if (_onStartup) {
                Start();
            }
        }

        public void UpdateConfig(Blob instanceData) {
            _updateModsOnStartup = instanceData.GetBool("updateModsOnStartup");
            _updateStaxelOnStartup = instanceData.GetBool("updateStaxelOnStartup");
            _onStartup = instanceData.GetBool("onStartup");
            _commandArgs = instanceData.GetString("commandArgs");
            _workshopMods = instanceData.GetList<int>("workshopMods");
            _name = instanceData.GetString("name");
        }

        public void UpdateStaxel() {
            var blob = new Blob();

            blob.SetString("type", Constants.MessageUpdateServerStatus);
            blob.SetString("id", ID.ToString());
            blob.SetString("status", "Installing/Updating Staxel");
            Server.SendBlob(blob);

            StaxelHostService.Log.Write($"Updating Staxel server {ID} ({_name})");

            _process = Process.Start(StaxelHostService.Settings.SteamCmdBin,
                $"+login {StaxelHostService.Settings.SteamUserName} +force_install_dir \"{Directory}\" +app_update {StaxelHostService.Settings.StaxelSteamId} validate +quit");

            _process?.WaitForExit();

            RunContentBuilder();

            blob.SetString("status", "Idle");
            Server.SendBlob(blob);
            blob.Dispose();
        }

        public void Start() {
            if (_process != null && !_process.HasExited) {
                return;
            }

            var forceInstall = !new FileInfo(Path.Combine(Directory, "bin", Constants.StaxelServerBin)).Exists;

            if (_updateStaxelOnStartup || forceInstall) {
                UpdateStaxel();
                RunContentBuilder();
            }

            if (_updateModsOnStartup || forceInstall) {
                DownloadMods();
                InstallMods();
            }

            var blob = new Blob();

            blob.SetString("type", Constants.MessageUpdateServerStatus);
            blob.SetString("id", ID.ToString());
            blob.SetString("status", "Running");
            Server.SendBlob(blob);
            blob.Dispose();

            StaxelHostService.Log.Write($"Starting Staxel server {ID} ({_name}) with the command lines: '{_commandArgs}'");

            _process = Process.Start(Path.Combine(Directory, "bin", Constants.StaxelServerBin), _commandArgs);

            if (_process != null) {
                _process.EnableRaisingEvents = true;
                _process.Exited += (sender, args) => {
                    Start();
                };
            }

            _canCancel = true;
        }

        public void RunContentBuilder() {
            var blob = new Blob();

            blob.SetString("type", Constants.MessageUpdateServerStatus);
            blob.SetString("id", ID.ToString());
            blob.SetString("status", "Content Building");
            Server?.SendBlob(blob);

            _canCancel = false;

            StaxelHostService.Log.Write($"Running content builder for {ID} ({_name}");

            _process = Process.Start(Path.Combine(Directory, "bin", Constants.StaxelContentBuilderBin), "--silent");
            _process?.WaitForExit();

            blob.SetString("status", "Idle");
            Server?.SendBlob(blob);

            blob.Dispose();

            if (StaxelHostService.Settings.EmptyRecycleBin) {
                SHEmptyRecycleBin(IntPtr.Zero,
                    null,
                    RecycleBinFlags.NoConfirmation | RecycleBinFlags.NoProgressUi | RecycleBinFlags.NoSound);
            }
        }

        public void DownloadMods() {
            var blob = new Blob();

            blob.SetString("type", Constants.MessageUpdateServerStatus);
            blob.SetString("id", ID.ToString());
            blob.SetString("status", "Downloading mods");
            Server?.SendBlob(blob);

            var sb = new StringBuilder();

            foreach (var id in _workshopMods) {
                sb.Append($"+workshop_download_item {StaxelHostService.Settings.StaxelSteamId} {id} ");
            }

            StaxelHostService.Log.Write($"Downloading {_workshopMods.Length} mods for {ID} ({_name}");

            _canCancel = false;
            _process = Process.Start(StaxelHostService.Settings.SteamCmdBin, $"+login {StaxelHostService.Settings.SteamUserName} +force_install_dir \"{Directory}\" {sb} +quit");

            _process?.WaitForExit();

            blob.SetString("status", "Idle");
            Server?.SendBlob(blob);
            blob.Dispose();
        }

        public void InstallMods() {
            var blob = new Blob();

            blob.SetString("type", Constants.MessageUpdateServerStatus);
            blob.SetString("id", ID.ToString());
            blob.SetString("status", "Uninstalling all mods");
            Server?.SendBlob(blob);

            _canCancel = false;

            StaxelHostService.Log.Write($"Removing all mods for {ID} ({_name})");

            _process = Process.Start(Path.Combine(Directory, "bin", Constants.StaxelModManagerBin), "--removeAllMods");

            _process?.WaitForExit();

            var packagesDir = new DirectoryInfo(Path.Combine(Directory, "LocalContent", "packages"));

            if (packagesDir.Exists) {
                packagesDir.Delete(true);
            }

            foreach (var id in _workshopMods) {
                var files = new DirectoryInfo(Path.Combine(Directory, "steamapps", "workshop", "content",
                    StaxelHostService.Settings.StaxelSteamId.ToString(), id.ToString())).GetFiles("*.sxlmod");

                foreach (var file in files) {
                    StaxelHostService.Log.Write($"Installing {file.Name} to {ID} {_name}");
                    blob.SetString("status", $"Installing {file.Name}");
                    Server?.SendBlob(blob);

                    _process = Process.Start(Path.Combine(Directory, "bin", Constants.StaxelModManagerBin),
                        $"--buildOnInstall=false --installMod {file.FullName}");

                    _process?.WaitForExit();
                }
            }

            blob.Dispose();

            RunContentBuilder();
        }

        public void Stop() {
            if (_process != null && _canCancel) {
                StaxelHostService.Log.Write($"Stopping {ID} {_name}");

                Process.Start("taskkill", "/PID " + _process.Id)?.WaitForExit();
                _process.WaitForExit();
                var blob = new Blob();

                blob.SetString("type", Constants.MessageUpdateServerStatus);
                blob.SetString("id", ID.ToString());
                blob.SetString("status", "Idle");
                Server?.SendBlob(blob);
                blob.Dispose();
                _process = null;
            }
        }

        public void SaveConfig() {
            var blob = new Blob();
            blob.SetString("name", _name);
            blob.SetBool("onStartup", _onStartup);
            blob.SetString("commandArgs", _commandArgs);
            blob.SetList("workshopMods", _workshopMods.ToList());
            blob.SetString("id", ID.ToString());
            blob.SetBool("updateModsOnStartup", _updateModsOnStartup);
            blob.SetBool("updateStaxelOnStartup", _updateStaxelOnStartup);

            StaxelHostService.Log.Write($"Updating config for {ID} {_name}");

            File.WriteAllText(Path.Combine(Constants.InstancesDirectory, ID + ".instance"), blob.ToString());
            blob.Dispose();
        }

        [DllImport("Shell32.dll", CharSet = CharSet.Unicode)]
        static extern uint SHEmptyRecycleBin(IntPtr hwnd, string rootPath, RecycleBinFlags flags);

        [Flags]
        enum RecycleBinFlags : uint {
            NoConfirmation = 0x00000001,
            NoProgressUi = 0x00000002,
            NoSound = 0x00000004
        }
    }
}
