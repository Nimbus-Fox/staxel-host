del /S /Q C:\WIP\staxel-host\publish\linux

dotnet publish C:\WIP\staxel-host\NimbusFox.Staxel.Host.NixService\ -c RELEASE -r linux-x64 -o C:\WIP\staxel-host\publish\linux\NixService
dotnet publish C:\WIP\staxel-host\NimbusFox.Staxel.Host.Web\ -c RELEASE -r linux-x64 -o C:\WIP\staxel-host\publish\linux\Web
dotnet publish C:\WIP\staxel-host\Staxel.Host.Installer -c Release -r linux-x64 -o C:\WIP\staxel-host\publish\linux\installer