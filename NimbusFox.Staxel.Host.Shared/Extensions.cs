﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NimbusFox.Staxel.Host.Shared {
    public static class Extensions {
        public static bool IsNullOrWhitespace(this string text) {
            return string.IsNullOrWhiteSpace(text);
        }
    }
}
