﻿using System;

namespace NimbusFox.Staxel.Host.Shared.Exceptions.BlobExceptions {
    public class BlobKindException : Exception {
        public BlobKindException() { }

        public BlobKindException(string message) : base(message) { }

        public BlobKindException(string message, Exception innerException) : base(message, innerException) { }
    }

    public class BlobValueException : Exception {
        public BlobValueException() { }

        public BlobValueException(string message) : base(message) { }

        public BlobValueException(string message, Exception innerException) : base(message, innerException) { }
    }
}
