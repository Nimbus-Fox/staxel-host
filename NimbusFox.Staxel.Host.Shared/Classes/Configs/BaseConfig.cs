﻿using System.IO;

namespace NimbusFox.Staxel.Host.Shared.Classes.Configs {
    public class BaseConfig {
        public DirectoryInfo LogDir { get; }
        public int ServicePort { get; }
        public BaseConfig(Blob config) {
            LogDir = new DirectoryInfo(config.GetString("logDirectory", "./"));
            ServicePort = config.GetInt("servicePort", 3000);
        }
    }
}
