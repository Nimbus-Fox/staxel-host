﻿using System.IO;

namespace NimbusFox.Staxel.Host.Shared.Classes.Configs {
    public class ServiceConfig : BaseConfig {
        public DirectoryInfo InstanceConfigDir { get; }
        public DirectoryInfo InstanceDir { get; }
        public bool RunWebServer { get; }
        public DirectoryInfo WebServerDir { get; }

        public ServiceConfig(Blob config) : base(config) {
            InstanceConfigDir = new DirectoryInfo(config.GetString("instanceConfigDir", "./"));
            InstanceDir = new DirectoryInfo(config.GetString("instanceDir"));
            RunWebServer = config.GetBool("runWebServer", false);

            if (RunWebServer) {
                WebServerDir = new DirectoryInfo(config.GetString("webServerDir"));
            }
        }
    }
}
