﻿using System;
using System.Collections.Generic;
using System.Net.Sockets;
using System.Text;
using Networker.Common;
using Networker.Formatter.ZeroFormatter;
using Networker.Server;
using Networker.Server.Abstractions;

namespace NimbusFox.Staxel.Host.Shared.Classes {
    public class CommunicationServer {
        private IServer Server;

        public CommunicationServer(int port) {
            Server = new ServerBuilder()
                .UseUdp(port)
                .UseLogger<ConsoleLogger>()
#if DEBUG
                .SetLogLevel(LogLevel.Debug)
#else
                .SetLogLevel(LogLevel.Info)
#endif
                .UseZeroFormatter()
                .Build();

            Server.Start();

            
        }
    }
}
