﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace NimbusFox.Staxel.Host.Shared.Classes {
    public sealed class Encryption {
        private RSACryptoServiceProvider EncryptionProvider;

        public Encryption(string privateKey) {
            EncryptionProvider = new RSACryptoServiceProvider(new CspParameters {ProviderType = 1});
            EncryptionProvider.ImportCspBlob(Convert.FromBase64String(privateKey));
        }

        private Encryption() {
            EncryptionProvider = new RSACryptoServiceProvider(2048, new CspParameters {ProviderType = 1});
        }

        public static Encryption GetEncryption(out string key) {
            var enc = new Encryption();

            key = Convert.ToBase64String(enc.EncryptionProvider.ExportCspBlob(true));

            return enc;
        }

        public Blob Decrypt(byte[] hash) {
            return Blob.ReadJson(
                Encoding.UTF8.GetString(EncryptionProvider.Decrypt(hash, RSAEncryptionPadding.OaepSHA512)));
        }

        public byte[] Encrypt(Blob value) {
            return EncryptionProvider.Encrypt(Encoding.UTF8.GetBytes(value.ToString()),
                RSAEncryptionPadding.OaepSHA512);
        }
    }
}
