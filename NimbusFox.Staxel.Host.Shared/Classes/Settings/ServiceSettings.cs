﻿namespace NimbusFox.Staxel.Host.Shared.Classes.Settings {
    public class ServiceSettings {
        public string SteamCmdBin => Config.GetString("steamCmdBin");
        public string ServerStorageLocation => Config.GetString("serverStorageLocation");
        public string ServiceName => Config.GetString("serviceName", "Nimbusfox's Staxel Server Service");
        public string SteamUserName => Config.GetString("steamUserName");
        public string PipeName => Config.GetString("pipeName", "staxelService");
        public int StaxelSteamId => Config.GetInt("staxelSteamId", 405710);
        public bool EmptyRecycleBin => Config.GetBool("emptyRecycleBin", false);
        public bool ValidSettings { get; } = true;

        private Blob Config { get; }

        public ServiceSettings(Blob config) {
            Config = config.Clone();

            try {
                Config.GetString("steamCmdBin");
                Config.GetString("serverStorageLocation");
                Config.GetString("steamUserName");
            } catch {
                ValidSettings = false;
            }
        }
    }
}
