﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NimbusFox.Staxel.Host.Shared.Classes.Settings {
    public class WebSettings {
        public Dictionary<string, string> DatabaseInfo { get; }
        public WebSettings(Blob config) {
            DatabaseInfo = config.GetBlob("databaseInfo").ToObject<Dictionary<string, string>>();
        }
    }
}
