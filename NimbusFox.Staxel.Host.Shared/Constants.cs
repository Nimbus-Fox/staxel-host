﻿using System.IO;

namespace NimbusFox.Staxel.Host.Shared {
    public class Constants {

        public static string InstancesDirectory => Path.Combine("", "Instances");

        public const string MessageCreateServer = "Nimbusfox.Staxel.Host.Service.CreateServer";
        public const string MessageDeleteServer = "Nimbusfox.Staxel.Host.Service.DeleteServer";
        public const string MessageUpdateServer = "Nimbusfox.Staxel.Host.Service.UpdateServer";
        public const string MessageUpdateServerConfig = "Nimbusfox.Staxel.Host.Service.UpdateServerConfig";
        public const string MessageUpdateServerStatus = "Nimbusfox.Staxel.Host.Service.UpdateServerStatus";

        public const string StaxelServerBin = "Staxel.Server.NoConsole.exe";
        public const string StaxelContentBuilderBin = "Staxel.Content.Builder.exe";
        public const string StaxelModManagerBin = "Staxel.Mod.Manager.exe";
    }
}
