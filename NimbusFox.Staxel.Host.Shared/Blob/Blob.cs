﻿using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace NimbusFox.Staxel.Host.Shared {
    public class Blob : IDisposable {
        public IReadOnlyDictionary<string, BlobEntry> Entries => _entries;
        private Dictionary<string, BlobEntry> _entries { get; } = new Dictionary<string, BlobEntry>();

        public void Dispose() {
            foreach (var entry in _entries) {
                if (entry.Value.Kind == BlobKind.Blob) {
                    entry.Value.GetBlob().Dispose();
                }
            }

            _entries.Clear();
        }

        private void ValidateKey(string key) {
            if (!_entries.ContainsKey(key)) {
                _entries.Add(key, new BlobEntry());
            }
        }

        public Blob SetString(string key, string value) {
            ValidateKey(key);
            _entries[key].SetString(value);
            return this;
        }

        public Blob SetBool(string key, bool value) {
            ValidateKey(key);
            _entries[key].SetBool(value);
            return this;
        }

        public Blob SetNumber(string key, long value) {
            ValidateKey(key);
            _entries[key].SetNumber(value);
            return this;
        }

        public Blob SetDouble(string key, double value) {
            ValidateKey(key);
            _entries[key].SetDouble(value);
            return this;
        }

        public Blob SetList<T>(string key, List<T> value) {
            ValidateKey(key);
            _entries[key].SetList(value);
            return this;
        }

        public Blob SetBlob(string key, Blob value) {
            ValidateKey(key);
            if (_entries[key].GetValue() is Blob blob) {
                blob.Dispose();
            }
            _entries[key].SetBlob(value);
            return this;
        }

        public Blob SetGuid(string key, Guid value) {
            ValidateKey(key);
            _entries[key].SetString(value.ToString());
            return this;
        }

        public Blob FetchBlob(string key) {
            ValidateKey(key);
            if (_entries[key].GetValue() == null) {
                _entries[key].SetBlob(new Blob());
            }
            return _entries[key].GetBlob();
        }

        public string GetString(string key) {
            return _entries[key].GetString();
        }

        public string GetString(string key, string _default) {
            return !_entries.ContainsKey(key) ? _default : _entries[key].GetString(_default);
        }

        public bool GetBool(string key) {
            return _entries[key].GetBool();
        }

        public bool GetBool(string key, bool _default) {
            return !_entries.ContainsKey(key) ? _default : _entries[key].GetBool(_default);
        }

        public long GetLong(string key) {
            return _entries[key].GetLong();
        }

        public long GetLong(string key, long _default) {
            return !_entries.ContainsKey(key) ? _default : _entries[key].GetLong(_default);
        }

        public uint GetUInt(string key) {
            return _entries[key].GetUInt();
        }

        public uint GetUInt(string key, uint _default) {
            return !_entries.ContainsKey(key) ? _default : _entries[key].GetUInt(_default);
        }

        public int GetInt(string key) {
            return _entries[key].GetInt();
        }

        public int GetInt(string key, int _default) {
            return !_entries.ContainsKey(key) ? _default : _entries[key].GetInt(_default);
        }

        public byte GetByte(string key) {
            return _entries[key].GetByte();
        }

        public byte GetByte(string key, byte _default) {
            return !_entries.ContainsKey(key) ? _default : _entries[key].GetByte(_default);
        }

        public decimal GetDecimal(string key) {
            return _entries[key].GetDecimal();
        }

        public decimal GetDecimal(string key, decimal _default) {
            return !_entries.ContainsKey(key) ? _default : _entries[key].GetDecimal(_default);
        }

        public float GetFloat(string key) {
            return _entries[key].GetFloat();
        }

        public float GetFloat(string key, float _default) {
            return !_entries.ContainsKey(key) ? _default : _entries[key].GetFloat(_default);
        }

        public double GetDouble(string key) {
            return _entries[key].GetDouble();
        }

        public double GetDouble(string key, double _default) {
            return !_entries.ContainsKey(key) ? _default : _entries[key].GetDouble(_default);
        }

        public IEnumerable<T> GetList<T>(string key) {
            return _entries[key].GetList<T>();
        }

        public IEnumerable<T> GetList<T>(string key, IEnumerable<T> _default) {
            return !_entries.ContainsKey(key) ? _default : _entries[key].GetList<T>(_default);
        }

        public Blob GetBlob(string key) {
            return _entries[key].GetBlob();
        }

        public Blob GetBlob(string key, Blob _default) {
            return !_entries.ContainsKey(key) ? _default : _entries[key].GetBlob(_default);
        }

        public Guid GetGuid(string key) {
            return Guid.Parse(_entries[key].GetString());
        }

        public Guid GetGuid(string key, Guid _default) {
            return Guid.Parse(!_entries.ContainsKey(key) ? _default.ToString()
                : _entries[key].GetString(_default.ToString()));
        }

        public override string ToString() {
            var data = new Dictionary<string, object>();
            GetJsonData(this, data);
            return JsonConvert.SerializeObject(data, Formatting.Indented);
        }

        public Blob Delete(string key) {
            if (_entries.ContainsKey(key)) {
                var entry = _entries[key];
                if (entry.Kind == BlobKind.Blob) {
                    entry.GetBlob().Dispose();
                }

                _entries.Remove(key);
            }
            return this;
        }

        private static void GetJsonData(Blob level, IDictionary<string, object> current) {
            foreach (var entry in level.Entries.OrderBy(x => x.Key)) {
                if (entry.Value.Kind != BlobKind.Blob) {
                    current.Add(entry.Key, entry.Value.GetValue());
                } else {
                    var data = new Dictionary<string, object>();
                    GetJsonData(entry.Value.GetBlob(), data);
                    current.Add(entry.Key, data);
                }
            }
        }

        public void MergeFrom(Blob blob) {
            ReadJson(blob.ToString());
        }

        public bool Contains(string key) {
            return _entries.ContainsKey(key);
        }

        public Blob Clone() {
            var clone = new Blob();

            clone.MergeFrom(ToString());

            return clone;
        }

        public void MergeFrom(string json) {
            var data = JsonConvert.DeserializeObject<Dictionary<string, object>>(json);

            Parse(data, this);
        }

        public static Blob ReadJson(string json) {
            var blob = new Blob();

            blob.MergeFrom(json);

            return blob;
        }

        private void Parse(object data, Blob level) {
            if (data is Dictionary<string, object> == false) {
                return;
            }

            foreach (var entry in (Dictionary<string, object>)data) {
                if (entry.Value is JObject obj) {
                    Parse(obj.ToObject<Dictionary<string, object>>(), level.FetchBlob(entry.Key));
                    continue;
                }

                if (entry.Value is JArray list) {
                    var pList = list.Cast<object>().ToList();
                    if (level.Contains(entry.Key)) {
                        var en = level.GetList<object>(entry.Key);
                        pList.AddRange(en);
                        level.SetList(entry.Key, pList);
                    } else {
                        level.SetList(entry.Key, pList);
                    }
                    pList.Clear();
                    continue;
                }

                if (entry.Value == null) {
                    level.SetString(entry.Key, null);
                    continue;
                }

                if (entry.Value is string str) {
                    level.SetString(entry.Key, str);
                    continue;
                }

                if (entry.Value is long lon) {
                    level.SetNumber(entry.Key, lon);
                    continue;
                }

                if (entry.Value is bool bol) {
                    level.SetBool(entry.Key, bol);
                    continue;
                }

                if (entry.Value is double dou) {
                    level.SetDouble(entry.Key, dou);
                    continue;
                }
            }
        }

        public Blob FromObject(object obj) {
            ReadJson(JsonConvert.SerializeObject(obj));
            return this;
        }

        public static Blob FromObj(object obj) {
            return new Blob().FromObject(obj);
        }

        public T ToObject<T>() {
            return JsonConvert.DeserializeObject<T>(ToString());
        }
    }
}
