﻿namespace NimbusFox.Staxel.Host.Shared {
    public enum BlobKind {
        String,
        Number,
        Boolean,
        Decimal,
        List,
        Blob
    }
}
