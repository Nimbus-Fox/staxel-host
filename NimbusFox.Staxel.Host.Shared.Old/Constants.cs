﻿using System.IO;
using System.Reflection;

namespace NimbusFox.Staxel.Host.Shared {
    public class Constants {
        public const string ServerPipePrefix = "Nimbusfox.Staxel.Host.Server.";

        public static string BinDirectory => Assembly.GetExecutingAssembly().Location.Substring(0,
            Assembly.GetExecutingAssembly().Location.LastIndexOf(Path.DirectorySeparatorChar));

        public static string InstancesDirectory => Path.Combine(BinDirectory, "Instances");

        public const string MessageCreateServer = "Nimbusfox.Staxel.Host.Service.CreateServer";
        public const string MessageDeleteServer = "Nimbusfox.Staxel.Host.Service.DeleteServer";
        public const string MessageUpdateServer = "Nimbusfox.Staxel.Host.Service.UpdateServer";
        public const string MessageUpdateServerConfig = "Nimbusfox.Staxel.Host.Service.UpdateServerConfig";
        public const string MessageUpdateServerStatus = "Nimbusfox.Staxel.Host.Service.UpdateServerStatus";

        public const string StaxelServerBin = "Staxel.Server.NoConsole.exe";
        public const string StaxelContentBuilderBin = "Staxel.Content.Builder.exe";
        public const string StaxelModManagerBin = "Staxel.Mod.Manager.exe";
    }
}
