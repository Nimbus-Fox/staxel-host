﻿using System;
using System.IO;
using System.Text;
using Console = System.Console;

namespace NimbusFox.Staxel.Host.Shared {
    public class Logger {
        public enum LogType {
            Info,
            Debug,
            Warn,
            Error
        }

        private string LogDir { get; }

        public Logger(string logLocation) {
            if (!Directory.Exists(logLocation)) {
                Directory.CreateDirectory(logLocation);
            }

            LogDir = logLocation;
        }

        private void WriteFile(string log) {
            File.AppendAllText(Path.Combine(LogDir, DateTime.Now.ToString("yyyy-MM-dd") + ".log"), log);
        }

        public void Write(string message, LogType type = LogType.Info, ConsoleColor? color = null) {
            var sb = new StringBuilder();

            if (color != null) {
                Console.ForegroundColor = color.Value;
            }

            sb.Append($"[{DateTime.Now:HH:mm:ss}]");

            Console.Write(sb);

            sb.Append("[");
            Console.Write("[");

            switch (type) {
                default:
                    Console.ForegroundColor = ConsoleColor.Cyan;
                    Console.Write("Info");
                    sb.Append("Info");
                    break;
                case LogType.Debug:
                    Console.ForegroundColor = ConsoleColor.Green;
                    Console.Write("Debug");
                    sb.Append("Debug");
                    break;
                case LogType.Warn:
                    Console.ForegroundColor = ConsoleColor.Yellow;
                    Console.Write("Warn");
                    sb.Append("Warn");
                    break;
                case LogType.Error:
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.Write("Error");
                    sb.Append("Error");
                    break;
            }

            Console.ResetColor();

            Console.Write("]");
            sb.Append("]");

            if (color != null) {
                Console.ForegroundColor = color.Value;
            }

            Console.WriteLine(message);
            sb.Append(message + Environment.NewLine);

            WriteFile(sb.ToString());

            Console.ResetColor();
        }
    }
}
