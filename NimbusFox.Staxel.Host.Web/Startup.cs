﻿using System.Collections.Generic;
using System.IO;
using System.Runtime.InteropServices;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.Internal;
using Microsoft.Extensions.DependencyInjection;
using NimbusFox.Staxel.Host.Shared;
using NimbusFox.Staxel.Host.Web.Helpers;

namespace NimbusFox.Staxel.Host.Web {
    public class Startup {
        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services) {
            services.AddMvc();
            services.AddSignalR();
            services.AddRouting();
            services.AddSession();
            services.AddResponseCompression();
            services.AddSession();
            
            services.AddWebOptimizer(pipeline => {
                pipeline.MinifyCssFiles();
                pipeline.MinifyJsFiles();
                var scss = new List<string>();

                string src = null;
#if DEBUG
                src = "./config/scss/bootstrap/variables.scss";
#else
                if (RuntimeInformation.IsOSPlatform(OSPlatform.Windows)) {
                    src = "C:\\config\\scss\\bootstrap\\variables.scss";
                } else if (RuntimeInformation.IsOSPlatform(OSPlatform.Linux)) {
                    src = "/config/scss/bootstrap/variables.scss";
                }
#endif

                if (!src.IsNullOrWhitespace()) {
                    if (File.Exists(src)) {
                        scss.Add(src);
                    }
                }

                scss.Add("/scss/Bootstrap/bootstrap.scss");
                pipeline.AddScssBundle("/css/bootstrap.css", scss.ToArray());
            });

            TranslationHelper.Init();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env) {
            if (env.IsDevelopment()) {
                app.UseDeveloperExceptionPage();
            }

            app.UseSession();
            app.UseResponseCompression();
            app.UseWebOptimizer();
            app.UseSession();

            GlobalHelper.SetwwwPath(env.WebRootPath);

            app.UseMvc(route => {
                route.MapRoute("defaultSmall", "{action}", new {controller = "Home", action = "Index"});

                route.MapRoute("default", "{controller}/{action}/{id?}", new {controller = "Home", action = "Index"});
            });
        }
    }
}
