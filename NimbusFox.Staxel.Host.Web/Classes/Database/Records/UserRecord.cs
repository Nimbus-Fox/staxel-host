﻿using System;
using LinqToDB;
using LinqToDB.Mapping;

namespace NimbusFox.Staxel.Host.Web.Classes.Database.Records {
    [Table(WebConstants.DbUsers)]
    internal class UserRecord : BaseRecord {
        private string _userName;

        [Column("username", Order = 1, DataType = DataType.Text)]
        internal string UserName {
            get {
                IsDisposed(nameof(UserName));

                return _userName;
            }
            set {
                IsDisposed(nameof(UserName));

                _userName = value;
            }
        }

        private string _email;

        [Column("email", Order = 2, DataType = DataType.Text)]
        internal string Email {
            get {
                IsDisposed(nameof(Email));

                return _email;
            }
            set {
                IsDisposed(nameof(Email));

                _email = value;
            }
        }

        private bool _activated = false;

        [Column("activated")]
        internal bool Activated {
            get {
                IsDisposed(nameof(Activated));

                return _activated;
            }
            set {
                IsDisposed(nameof(Activated));

                _activated = value;
            }
        }

        public string _validationKey;

        [Column("validationKey", DataType = DataType.Text)]
        internal string ValidationKey {
            get {
                IsDisposed(nameof(ValidationKey));

                return _validationKey;
            }
            set {
                IsDisposed(nameof(ValidationKey));

                _validationKey = value;
            }
        }


    }
}
