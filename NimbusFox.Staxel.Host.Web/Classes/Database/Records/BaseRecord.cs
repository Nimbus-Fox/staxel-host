﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LinqToDB.Mapping;

namespace NimbusFox.Staxel.Host.Web.Classes.Database.Records {
    internal class BaseRecord : IDisposable {
        private bool _isDisposed;

        private long _id;

        [Column("id", Order = 0)]
        [PrimaryKey, Identity]
        internal long ID {
            get {
                IsDisposed(nameof(ID));

                return _id;
            }
            set {
                IsDisposed(nameof(ID));

                _id = value;
            }
        }

        protected void IsDisposed(string name) {
            if (_isDisposed) {
                throw new ObjectDisposedException(name);
            }
        }

        public void Dispose() {
            _isDisposed = true;
        }
    }
}
