﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LinqToDB.Configuration;

namespace NimbusFox.Staxel.Host.Web.Classes.Database {
    internal class DatabaseSettings : ILinqToDBSettings {
        /// <summary>Gets list of data provider settings.</summary>
        public IEnumerable<IDataProviderSettings> DataProviders { get; }

        /// <summary>Gets name of default connection configuration.</summary>
        public string DefaultConfiguration => "Sqlite";

        /// <summary>Gets name of default data provider configuration.</summary>
        public string DefaultDataProvider => "sqlite";

        /// <summary>Gets list of connection configurations.</summary>
        public IEnumerable<IConnectionStringSettings> ConnectionStrings { get; }

        public static string Name { get; private set; }
        public static string Provider { get; private set; }

        public DatabaseSettings(IReadOnlyDictionary<string, string> info) {
            var con = new ConnectionStringSettings {
                ConnectionString = info["connectionString"], ProviderName = info["providerName"],
                Name = info["name"]
            };

            Name = info["Name"];
            Provider = info["providerName"];

            ConnectionStrings = new IConnectionStringSettings[] {con};
        }
    }
}
