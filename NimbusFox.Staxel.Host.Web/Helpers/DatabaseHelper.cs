﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using NimbusFox.Staxel.Host.Web.Classes.Database;

namespace NimbusFox.Staxel.Host.Web.Helpers {
    internal static class DatabaseHelper {

        private static DatabaseConnection _singleConnection;

        static DatabaseHelper() {
            LinqToDB.Data.DataConnection.DefaultSettings = new DatabaseSettings(GlobalHelper.Settings.DatabaseInfo);
            GlobalHelper.Settings.DatabaseInfo.Clear();

            AccessDatabase(connection => {

            });


        }

        internal static void AccessDatabase(Action<DatabaseConnection> usage, bool isDisposeHandled = false) {
            DatabaseConnection connection;
            if (DatabaseSettings.Provider.ToLower() == "sqlce" || DatabaseSettings.Provider.ToLower() == "sqlite") {
                if (_singleConnection == null) {
                    _singleConnection = new DatabaseConnection(DatabaseSettings.Name);
                }

                connection = _singleConnection;
            } else {
                connection = new DatabaseConnection(DatabaseSettings.Name);
            }

            usage(connection);

            if (!isDisposeHandled) {
                connection.Dispose();
            }
        }
    }
}