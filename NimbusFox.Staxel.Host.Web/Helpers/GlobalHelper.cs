﻿using System;
using System.IO;
using System.Runtime.InteropServices;
using System.Security.Cryptography;
using System.Text;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using NimbusFox.Staxel.Host.Shared;
using NimbusFox.Staxel.Host.Shared.Classes.Settings;
using SharpScss;

namespace NimbusFox.Staxel.Host.Web.Helpers {
    internal static class GlobalHelper {
        private static ILoggerFactory LoggerFactory { get; }
        internal static WebSettings Settings { get; }
        private static ScssResult? BootstrapScss { get; set; }
        internal static string WwwRootPath { get; private set; }
        internal static DirectoryInfo ErrorDirectory { get; private set; }
        internal static DirectoryInfo ConfigDirectory { get; private set; }

        static GlobalHelper() {
#if DEBUG
            Settings = new WebSettings(Blob.ReadJson(File.ReadAllText("./config/web.json")));
            ConfigDirectory = new DirectoryInfo("./config");
#else
            if (RuntimeInformation.IsOSPlatform(OSPlatform.Linux)) {
                Settings = new WebSettings(Blob.ReadJson(File.ReadAllText("/config/web.json")));
                ConfigDirectory = new DirectoryInfo("/config");
            } else if (RuntimeInformation.IsOSPlatform(OSPlatform.Windows)) {
                Settings = new WebSettings(Blob.ReadJson(File.ReadAllText("C:\\config\\web.json")));
                ConfigDirectory = new DirectoryInfo("C:\\config");
            }
#endif

            LoggerFactory = new LoggerFactory();
            LoggerFactory.AddConsole();
            
            ErrorDirectory = new DirectoryInfo(Path.GetFullPath(Path.Combine(ConfigDirectory.FullName, "Errors")));
        }

        internal static ILogger<T> GetLogger<T>(this T obj) {
            return LoggerFactory.CreateLogger<T>();
        }

        internal static string GenerateSaltedHash(byte[] plainText, byte[] salt) {
            HashAlgorithm algorithm = new SHA256Managed();

            var plainTextWithSaltBytes =
                new byte[plainText.Length + salt.Length];

            for (var i = 0; i < plainText.Length; i++) {
                plainTextWithSaltBytes[i] = plainText[i];
            }

            for (var i = 0; i < salt.Length; i++) {
                plainTextWithSaltBytes[plainText.Length + i] = salt[i];
            }

            return Convert.ToBase64String(algorithm.ComputeHash(plainTextWithSaltBytes));
        }

        internal static string GenerateSaltedHash(string text, string salt) {
            return GenerateSaltedHash(Encoding.UTF8.GetBytes(text), Convert.FromBase64String(salt));
        }

        internal static string GenerateSalt(int size) {
            var random = new RNGCryptoServiceProvider();

            var salt = new byte[size];

            random.GetNonZeroBytes(salt);

            return Convert.ToBase64String(salt);
        }

        internal static void SetwwwPath(string path) {
            if (WwwRootPath == null) {
                WwwRootPath = path;
            }
        }

        internal static void HandleTypeException<T>(Exception ex) {
            if (!ErrorDirectory.Exists) {
                ErrorDirectory.Create();
            }

            File.WriteAllTextAsync(Path.Combine(ErrorDirectory.FullName, $"{DateTime.UtcNow.Ticks}.error"),
                JsonConvert.SerializeObject(ex, Formatting.Indented));

            LoggerFactory.CreateLogger<T>().LogError(ex, ex.Message);
        }

        internal static void HandleException(Exception ex, object obj) {
            if (!ErrorDirectory.Exists) {
                ErrorDirectory.Create();
            }

            File.WriteAllTextAsync(Path.Combine(ErrorDirectory.FullName, $"{DateTime.UtcNow.Ticks}.error"),
                JsonConvert.SerializeObject(ex, Formatting.Indented));

            obj.GetLogger().LogError(ex, ex.Message);
        }
    }
}
