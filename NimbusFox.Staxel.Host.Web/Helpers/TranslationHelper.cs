﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using NimbusFox.Staxel.Host.Shared;
using NimbusFox.Staxel.Host.Web.Translations;

namespace NimbusFox.Staxel.Host.Web.Helpers {
    internal class TranslationHelper {
        private static Dictionary<string, Dictionary<string, string>> _translations = new Dictionary<string, Dictionary<string, string>>();
        internal static IReadOnlyDictionary<string, string> Cultures { get; private set; }

        internal static string DefaultCulture { get; private set; }

        internal static void Init() {
            if (_translations.Count != 0) {
                return;
            }

            var cultures = new Dictionary<string, string>();

            var configDir = new DirectoryInfo(Path.Combine(GlobalHelper.ConfigDirectory.FullName, "translations"));

            var extras = new List<FileInfo>();

            if (configDir.Exists) {
                extras.AddRange(configDir.GetFiles("*.json"));
            }

            foreach (var file in new DirectoryInfo("./Translations").GetFiles("*.json")
                .Union(extras)) {
                try {
                    using (var blob = Blob.ReadJson(File.ReadAllText(file.FullName))) {
                        if (blob.Contains("code")) {
                            if (!_translations.ContainsKey(blob.GetString("code"))) {
                                _translations.Add(blob.GetString("code"), new Dictionary<string, string>());
                            }

                            var code = blob.GetString("code");

                            blob.Delete("code");

                            if (blob.GetBool("default", false)) {
                                DefaultCulture = code;
                            }

                            if (blob.Contains("default")) {
                                blob.Delete("default");
                            }

                            if (!cultures.ContainsKey(code)) {
                                cultures.Add(code,
                                    blob.GetString(TranslationConstants.Translation.Name,
                                        TranslationConstants.Translation.Name + "." + code));
                            } else {
                                if (blob.Contains(TranslationConstants.Translation.Name)) {
                                    cultures[code] = blob.GetString(TranslationConstants.Translation.Name);
                                }
                            }

                            foreach (var entry in blob.Entries) {
                                if (!_translations[code].ContainsKey(entry.Key)) {
                                    _translations[code].Add(entry.Key, entry.Value.GetString());
                                } else {
                                    _translations[code][entry.Key] = entry.Value.GetString();
                                }
                            }
                        }
                    }
                } catch (Exception ex) when (!System.Diagnostics.Debugger.IsAttached) {
                    GlobalHelper.HandleTypeException<TranslationHelper>(ex);
                }
            }

            Cultures = cultures;
        }

        internal static (string text, bool valid) GetTranslation(string code, string culture, params object[] parameters) {
            if (_translations.ContainsKey(culture)) {
                if (_translations[culture].TryGetValue(code, out var text)) {
                    return (string.Format(text, parameters), true);
                }
            }

            return (code + "." + culture, false);
        }
    }
}
