﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.AspNetCore.Http;
using NimbusFox.Staxel.Host.Shared;
using NimbusFox.Staxel.Host.Web.Helpers;
using NimbusFox.Staxel.Host.Web.Translations;

namespace NimbusFox.Staxel.Host.Web {
    public static class WebExtensions {
        public static string GetPageTitle(this HttpContext context) {
            SessionBlobCheck(context.Session);

            using (var blob = context.Session.GetBlob("context")) {
                if (!blob.Contains("title")) {
                    return "";
                }

                if (blob.Entries["title"].Kind != BlobKind.List) {
                    return "";
                }

                var list = blob.GetList<string>("title");

                var title = new StringBuilder();

                foreach (var item in list) {
                    if (item.IsNullOrWhitespace()) {
                        continue;
                    }
                    if (!title.ToString().IsNullOrWhitespace()) {
                        title.Append(context.GetTranslation(TranslationConstants.Site.Title.Separator).translation);
                    }

                    var (translation, valid) = context.GetTranslation(item);

                    title.Append(!valid ? item : translation);
                }

                return title.ToString();
            }
        }

        internal static void ClearTitle(this HttpContext context) {
            SessionBlobCheck(context.Session);

            using (var blob = context.Session.GetBlob("context")) {
                if (blob.Contains("title")) {
                    blob.Delete("title");
                }

                context.Session.SetBlob("context", blob);
            }
        }

        public static void AppendTitle(this HttpContext context, string translation) {
            SessionBlobCheck(context.Session);

            using (var blob = context.Session.GetBlob("context")) {
                if (!blob.Contains("title")) {
                    blob.SetList("title", new List<string>());
                }

                var list = blob.GetList<string>("title").ToList();

                list.Add(translation);

                blob.SetList("title", list);

                context.Session.SetBlob("context", blob);
            }
        }

        private static void SessionBlobCheck(ISession session) {
            if (!session.Contains("context")) {
                session.SetBlob("context", new Blob());
            }
        }

        internal static string GetCulture(this HttpContext context) {
            var cookies = context.Request.HttpContext.Request.Cookies;

            var output = TranslationHelper.DefaultCulture;

            foreach (var cookie in cookies) {
                if (cookie.Key == "culture") {
                    output = cookie.Value;
                    break;
                }
            }

            return output;
        }

        internal static (string translation, bool valid) GetTranslation(this HttpContext context, string code, params object[] parameters) {
            return TranslationHelper.GetTranslation(code, context.GetCulture(), parameters);
        }

        internal static void SetCulture(this HttpContext context, string culture) {
            if (TranslationHelper.Cultures.ContainsKey(culture)) {
                context.Response.Cookies.Append("culture", culture, new CookieOptions {
                    Expires = DateTime.Now.AddYears(1),
                    HttpOnly = true
                });
            }
        }

        internal static Blob GetBlob(this ISession session, string key) {
            if (!session.Contains(key)) {
                return new Blob();
            }

            try {
                return Blob.ReadJson(session.GetString(key));
            } catch {
                return new Blob();
            }
        }

        internal static void SetBlob(this ISession session, string key, Blob value) {
            session.SetString(key, value.ToString());
        }

        internal static bool Contains(this ISession session, string key) {
            return session.Keys.Contains(key);
        }
    }
}
