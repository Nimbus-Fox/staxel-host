﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NimbusFox.Staxel.Host.Web.Translations {
    public class TranslationConstants {
        public const string Code = "code";

        public class Translation {
            public const string Name = "translation.name";
        }

        public class Site {
            public class Title {
                public const string Separator = "site.title.separator";
                public const string Home = "site.title.home";
            }
        }
    }
}
