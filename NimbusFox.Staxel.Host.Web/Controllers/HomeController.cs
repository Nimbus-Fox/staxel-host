﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace NimbusFox.Staxel.Host.Web.Controllers {
    public class HomeController : Controller {
        public async Task<IActionResult> Index() {
            return await Task.Run(() => {
                HttpContext.ClearTitle();
                HttpContext.AppendTitle("site.title");
                HttpContext.AppendTitle("site.title.home");
                return View();
            });
        }
    }
}