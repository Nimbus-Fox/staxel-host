del /S /Q C:\WIP\staxel-host\publish\windows

dotnet publish C:\WIP\staxel-host\NimbusFox.Staxel.Host.Web\ -c RELEASE -r win-x64 -o C:\WIP\staxel-host\publish\windows\Web
dotnet publish C:\WIP\staxel-host\Staxel.Host.Installer -c Release -r win-x64 -o C:\WIP\staxel-host\publish\windows\installer

"C:\Program Files (x86)\Microsoft Visual Studio\2017\Community\MSBuild\15.0\Bin\MSBuild.exe" /restore:true /t:Rebuild /p:Configuration=Release C:\WIP\staxel-host\NimbusFox.Staxel.Host.WindowsService\NimbusFox.Staxel.Host.WindowsService.csproj

mkdir C:\WIP\staxel-host\publish\windows\WindowsService

robocopy /MIR C:\WIP\staxel-host\NimbusFox.Staxel.Host.WindowsService\bin\Release C:\WIP\staxel-host\publish\windows\WindowsService