﻿using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Threading;
using Konsole;
using NimbusFox.Staxel.Host.Shared;

namespace Staxel.Host.Installer {
    internal static class Program {
        private static void Logo() {
            Console.BackgroundColor = ConsoleColor.Black;
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("╔═╗┌┬┐┌─┐─┐ ┬┌─┐┬    ╦ ╦┌─┐┌─┐┌┬┐  ╦┌┐┌┌─┐┌┬┐┌─┐┬  ┬  ┌─┐┬─┐");
            Console.WriteLine("╚═╗ │ ├─┤┌┴┬┘├┤ │    ╠═╣│ │└─┐ │   ║│││└─┐ │ ├─┤│  │  ├┤ ├┬┘");
            Console.WriteLine("╚═╝ ┴ ┴ ┴┴ └─└─┘┴─┘  ╩ ╩└─┘└─┘ ┴   ╩┘└┘└─┘ ┴ ┴ ┴┴─┘┴─┘└─┘┴└─");
            Console.WriteLine();
            Console.ForegroundColor = ConsoleColor.DarkYellow;
            Console.WriteLine("Copyright 2018 NimbusFox");
            Console.WriteLine("This product and Nimbusfox are not affiliated with Staxel or Plukit");
            Console.ResetColor();
        }

        private static bool ExistsOnPath(string fileName) {
            return GetFullPath(fileName) != null;
        }

        private static string GetFullPath(string fileName) {
            if (File.Exists(fileName))
                return Path.GetFullPath(fileName);

            var values = Environment.GetEnvironmentVariable("PATH");
            return values?.Split(';').Select(path => Path.Combine(path, fileName)).FirstOrDefault(fullPath => File.Exists(fullPath) || File.Exists(fullPath + ".exe"));
        }

        private static void ShowError(string message, bool readKey = true) {
            Console.BackgroundColor = ConsoleColor.Black;
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine(message);
            Console.Write("Press any key to continue...");
            Console.ResetColor();
            if (readKey) {
                Console.ReadKey();
            }
        }

        private static void Main(string[] args) {
            var correct = Assembly.GetAssembly(typeof(Program)).Location;
            if (Directory.GetCurrentDirectory() != correct.Substring(0, correct.LastIndexOf(Path.DirectorySeparatorChar))) {
                Directory.SetCurrentDirectory(correct.Substring(0, correct.LastIndexOf(Path.DirectorySeparatorChar)));
                Console.ReadKey();
            }
            if (!RuntimeInformation.IsOSPlatform(OSPlatform.Windows) &&
                !RuntimeInformation.IsOSPlatform(OSPlatform.Linux)) {
                ShowError("Your OS is not supported. Staxel host only works with windows and linux");
                return;
            }

            if (!ExistsOnPath("docker")) {
                ShowError("Docker is not installed. Docker is required to run these services");
                return;
            }

            while (true) {
                Menu();
            }
        }

        private static void Menu() {
            Console.Clear();
            Logo();
            Console.WriteLine();

            Console.WriteLine("1. Install Web Service");
            Console.WriteLine("2. Install Instance Service");
            Console.WriteLine("3. Update Services");
            Console.WriteLine("4. Quit");
            Console.WriteLine();
            Console.Write("Choose an option: ");

            var choice = Console.ReadKey(false);
            if (choice.Key == ConsoleKey.D1) {
                WebService();
            } else if (choice.Key == ConsoleKey.D2) {
                InstanceService();
            } else if (choice.Key == ConsoleKey.D3) {
                WebService(true);
                InstanceService(true);
            } else if (choice.Key == ConsoleKey.D4) {
                Environment.Exit(0);
            }
        }

        private static void WebService(bool pullOnly = false) {
            Console.Clear();
            Logo();
            Console.WriteLine();
            ProcessStartInfo si = null;
            if (RuntimeInformation.IsOSPlatform(OSPlatform.Linux)) {
                si = new ProcessStartInfo("docker", "pull nimbusfox6996/staxel-host-web-nix");
            } else if (RuntimeInformation.IsOSPlatform(OSPlatform.Windows)) {
                si = new ProcessStartInfo("docker.exe", "pull nimbusfox6996/staxel-host-web-win");
            }

            if (si != null) {
                var process = new Process { StartInfo = si };

                process.Start();

                Console.WriteLine("Pulling " + si.Arguments.Substring(5));

                process.WaitForExit();

                if (pullOnly) {
                    return;
                }

                Console.Clear();
                Logo();
                Console.WriteLine();

                var configDir = Path.Combine(new DirectoryInfo("./").Parent.FullName, "webconfig");
                var port = 8080;
                var ssl = false;

                configDir:
                Console.Write($"WebConfig Directory({configDir}): ");
                var entry = Console.ReadLine();

                if (!entry.IsNullOrWhitespace()) {
                    try {
                        Path.GetFullPath(entry);
                        Directory.CreateDirectory(entry);
                        Directory.Delete(entry);
                        configDir = new DirectoryInfo(entry).FullName;
                    } catch {
                        Console.CursorLeft = 0;
                        Console.Write(new string(' ', Console.WindowWidth));
                        Console.CursorLeft = 0;
                        goto configDir;
                    }
                }

                port:

                Console.WriteLine();
                Console.Write($"Port({port}): ");
                entry = Console.ReadLine();

                if (!entry.IsNullOrWhitespace()) {
                    if (int.TryParse(entry, out var portNum)) {
                        if (portNum >= 0 && portNum <= 65535) {
                            port = portNum;
                        } else {
                            Console.CursorLeft = 0;
                            Console.Write(new string(' ', Console.WindowWidth));
                            Console.CursorLeft = 0;
                            goto port;
                        }
                    } else {
                        Console.CursorLeft = 0;
                        Console.Write(new string(' ', Console.WindowWidth));
                        Console.CursorLeft = 0;
                        goto port;
                    }
                }


            }
        }

        private static void InstanceService(bool pullOnly = false) {
            Console.Clear();
            Logo();
            Console.WriteLine();
            var url = "";

            if (RuntimeInformation.IsOSPlatform(OSPlatform.Linux)) {
                url = "https://jenkins.nimbusfox.uk/job/Staxel-Host/lastSuccessfulBuild/artifact/publish/linux.zip";
            } else if (RuntimeInformation.IsOSPlatform(OSPlatform.Windows)) {
                url = "https://jenkins.nimbusfox.uk/job/Staxel-Host/lastSuccessfulBuild/artifact/publish/windows.zip";
            }

            if (File.Exists("./download.zip")) {
                File.Delete("./download.zip");
            }

            var webClient = new WebClient();

            var progress = new ProgressBar(0);

            var lastProgress = 0L;
            var total = 0L;
            var lastPercent = 0;
            var update = false;

            webClient.DownloadProgressChanged += (sender, e) => {
                progress.Max = (int)e.TotalBytesToReceive;
                total = e.TotalBytesToReceive;
                if (lastProgress < e.BytesReceived) {
                    lastProgress = e.BytesReceived;
                }

                if (lastPercent < e.ProgressPercentage) {
                    lastPercent = e.ProgressPercentage;
                    update = true;
                }
            };

            webClient.DownloadFileCompleted += (sender, e) => {
                webClient.Dispose();
                webClient = null;
            };

            webClient.DownloadFileAsync(new Uri(url), "./download.zip");

            while (webClient != null) {
                if (update) {
                    progress.Refresh((int)lastProgress, url.Substring(url.LastIndexOf('/') + 1) + $" {lastProgress}/{total}");
                    update = false;
                    Thread.Sleep(200);
                }
            }

            if (pullOnly) {
                return;
            }

            Console.Clear();
            Logo();
            Console.WriteLine();
        }
    }
}
