﻿using System.ServiceProcess;

namespace NimbusFox.Staxel.Host.WindowsService {
    public partial class Service : ServiceBase {
        public Service() {
            InitializeComponent();
        }

        protected override void OnStart(string[] args) {
        }

        protected override void OnStop() {
        }
    }
}
